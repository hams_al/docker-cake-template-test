# Docker-CakePHP(手始めに) #


## ファイル構成 ##
```
doker-cake-template-test
　├ html
　│　└ index.php
　├ .gitignore
　├ docker-compose.yml
　└ README.md
```

---

## コマンド類 ##

### 起動 ###

※phpのイメージがダウンロードされる。

```
$ docker-compose up -d
```

### 起動確認 ###

```
$ docker-compose ps
```

### ブラウザから確認 ###

```
http://localhost/
```

### 停止 ###

```
$ docker-compose stop
```

### 後かたずけ ###

```
$ docker-compose down
```


